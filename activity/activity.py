from abc import ABC, abstractmethod

# Abstract class
class Animal(ABC):

    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass

# Dog class
class Dog(Animal):

    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self, food):
        print("Eaten", food)

    def make_sound(self):
        print("Bark! Woof! Arf!")

    def call(self):
        print("Here", self._name + "!")

    # getters
    @property
    def name(self):
        return self._name

    @property
    def breed(self):
        return self._breed

    @property
    def age(self):
        return self._age

    # setters
    @name.setter
    def name(self, name):
        self._name = name

    @breed.setter
    def breed(self, breed):
        self._breed = breed

    @age.setter
    def age(self, age):
        self._age = age


# Cat class
class Cat(Animal):

    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self, food):
        print("Serve me", food)

    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaaa!")

    def call(self):
        print(self._name + ", come on!")

    # getters
    @property
    def name(self):
        return self._name

    @property
    def breed(self):
        return self._breed

    @property
    def age(self):
        return self._age

    # setters
    @name.setter
    def name(self, name):
        self._name = name

    @breed.setter
    def breed(self, breed):
        self._breed = breed

    @age.setter
    def age(self, age):
        self._age = age


# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
